# 
# Translators:
# Luke Horwell <lukehorwell37+code@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-04-03 16:26+0000\n"
"Last-Translator: Luke Horwell <lukehorwell37+code@gmail.com>\n"
"Language-Team: English (United Kingdom) (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/en_GB/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en_GB\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: donate.html:17
msgid "Donate"
msgstr "Donate"

#: donate.html:25
msgid "About Donations"
msgstr "About Donations"

#: donate.html:26
msgid "Our Supporters"
msgstr "Our Supporters"

#: donate.html:29
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr "It appears you are not connected to the Internet. Please check your connection to access this content."

#: donate.html:30
msgid "Sorry, Welcome was unable to establish a connection."
msgstr "Sorry, Welcome was unable to establish a connection."

#: donate.html:31
msgid "Retry"
msgstr "Retry"

#: donate.html:40
msgid ""
"Ubuntu MATE is funded by our community. Your donations help pay towards:"
msgstr "Ubuntu MATE is funded by our community. Your donations help pay towards:"

#: donate.html:42
msgid "Web hosting and bandwidth costs."
msgstr "Web hosting and bandwidth costs."

#: donate.html:43
msgid "Supporting Open Source projects that Ubuntu MATE depends upon."
msgstr "Supporting Open Source projects that Ubuntu MATE depends upon."

#: donate.html:44
msgid ""
"Eventually pay for full time developers of Ubuntu MATE and MATE Desktop."
msgstr "Eventually pay for full time developers of Ubuntu MATE and MATE Desktop."

#: donate.html:46
msgid "Patreon"
msgstr "Patreon"

#: donate.html:46
msgid ""
"is a unique way to fund an Open Source project. A regular monthly income "
"ensures sustainability for the Ubuntu MATE project. Patrons will be rewarded"
" with exclusive project news, updates and invited to participate in video "
"conferences where you can talk to the developers directly."
msgstr "is a unique way to fund an Open Source project. A regular monthly income ensures sustainability for the Ubuntu MATE project. Patrons will be rewarded with exclusive project news, updates and invited to participate in video conferences where you can talk to the developers directly."

#: donate.html:51
msgid "If you would prefer to use"
msgstr "If you would prefer to use"

#: donate.html:51
msgid ""
"then we have options to donate monthly or make a one off donation. Finally, "
"we also accept donations via"
msgstr "then we have options to donate monthly or make a one off donation. Finally, we also accept donations via"

#: donate.html:53
msgid "Bitcoin"
msgstr "Bitcoin"

#: donate.html58, 71
msgid "Become a Patron"
msgstr "Become a Patron"

#: donate.html:61
msgid "Donate with PayPal"
msgstr "Donate with PayPal"

#: donate.html:64
msgid "Donate with Bitcoin"
msgstr "Donate with Bitcoin"

#: donate.html:70
msgid "Commercial sponsorship"
msgstr "Commercial sponsorship"

#: donate.html:70
msgid "is also available. Click the"
msgstr "is also available. Click the"

#: donate.html:71
msgid "button above to find out more."
msgstr "button above to find out more."

#: donate.html:79
msgid "Thank you!"
msgstr "Thank you!"

#: donate.html:80
msgid ""
"Every month, we compile a list of our donations and detail how they are "
"spent."
msgstr "Every month, we compile a list of our donations and detail how they are spent."

#: donate.html:81
msgid "This information is published monthly to the"
msgstr "This information is published monthly to the"

#: donate.html:81
msgid "Ubuntu MATE Blog"
msgstr "Ubuntu MATE Blog"

#: donate.html:82
msgid "which you can take a look below."
msgstr "which you can take a look below."

#: donate.html:88
msgid "Year"
msgstr "Year"

#: donate.html:89
msgid "Month"
msgstr "Month"
